<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmailController extends Controller
{

    public function send(Request $request){
        $subject = $request['subject'];
        $content = $request['content'];
        $to = $request['email'];

        \Mail::send('email.sent_email', ['subject' => $subject, 'content' => $content], function ($message) use($to)
        {

            $message->from('bikram.skaplink@gmail.com', 'Bikram');

            $message->to($to);

        });
        $notification = array(
            'message' => 'I am a successful message!', 
            'alert-type' => 'success'
        );
        

        return redirect('/home')->with($notification);
    }
}
